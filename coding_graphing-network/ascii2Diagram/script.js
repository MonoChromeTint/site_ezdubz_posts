function save() {
    var svg = document.getElementById("yes");
    var serializer = new XMLSerializer();
    var source = serializer.serializeToString(svg);
    var url = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(source);
    var abc = document.createElement("a");


    abc.href = url;
    abc.download = "graph.svg";
    document.body.appendChild(abc);

    abc.click();

    document.body.removeChild(abc);
}


function launch() {
    var xhr = new XMLHttpRequest();
    let yesNo = document.getElementById('textID').value;
    let yourServer = document.getElementById('serverLOC').value;

    if (!yourServer || !yourServer.length || !(yourServer.trim())) {
        let aAa = document.getElementById('yes');
        aAa.innerHTML = "ERROR: Cannot be empty";
    } else {
        xhr.open("POST", yourServer, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = (e) => {
            let aAa = document.getElementById('yes');
            aAa.innerHTML = xhr.responseText;
            console.log(xhr.responseText);
        }
        xhr.send(JSON.stringify({
            "diagram_source": yesNo,
            "diagram_type": "graphviz",
            "output_format": "svg"
        }));
    }
}




let textArea = document.getElementById('textID');

function keyCombos(e) {
    let evtobj = window.event ? event : e
    let val = document.getElementById('text1');
    if (evtobj.keyCode == 83 && evtobj.crtlKey) {
        e.preventDefault();
        launch()
    }
    else if(evtobj.keyCode == 83 && evtobj.altKey) {
        e.preventDefault();
        launch()

    }
}

document.getElementById('textID').onkeydown = keyCombos;
