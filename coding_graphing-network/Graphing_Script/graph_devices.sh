#!/bin/bash

## Run On OpenWrt:
# arp > tmpARP; for i in $(cat tmpARP | tr -s ' ' | tr ' ' ',' | sort -V -t',' -k3 -k1 | grep -Ev '0x0'); do MATHC=$(grep "$(echo $i | cut -d',' -f1)" /tmp/dhcp.leases); echo "$(echo $MATHC | cut -d' ' -f4),$i"; done > arpList

## Copy from OpenWrt
# scp Router:arpList .

SetColorC=""
function colorC {
	if [[ "$1" == "br-lan.1" ]]; then SetColorC="green"; fi
	if [[ "$1" == "br-lan.10" ]]; then SetColorC="purple"; fi
	if [[ "$1" == "br-lan.12" ]]; then SetColorC="lightgreen"; fi
	if [[ "$1" == "br-lan.13" ]]; then SetColorC="lightblue"; fi
	if [[ "$1" == "br-lan.15" ]]; then SetColorC="darkorange"; fi
	if [[ "$1" == "br-lan.30" ]]; then SetColorC="darkblue"; fi
	if [[ "$1" == "br-lan.50" ]]; then SetColorC="red"; fi
	if [[ "$1" == "br-lan.72" ]]; then SetColorC="black"; fi
}



nameOhName=""
function nameON {
	if [[ "$1" == "eth1" ]]; then nameOhName="WAN"; fi
	if [[ "$1" == "br-lan.1" ]]; then nameOhName="Lan"; fi
	if [[ "$1" == "br-lan.10" ]]; then nameOhName="Management Devices"; fi
	if [[ "$1" == "br-lan.12" ]]; then nameOhName="Proxmox Servers"; fi
	if [[ "$1" == "br-lan.13" ]]; then nameOhName="Swarm Nodes"; fi
	if [[ "$1" == "br-lan.15" ]]; then nameOhName="Switches"; fi
	if [[ "$1" == "br-lan.30" ]]; then nameOhName="Windows Machines"; fi
	if [[ "$1" == "br-lan.50" ]]; then nameOhName="Wireless Devices"; fi
	if [[ "$1" == "br-lan.72" ]]; then nameOhName="Test Plates"; fi
}



ROOT_NAME="Cluster_Devices"

FileLOC="arpList.example"

RouterBase="eth1"
SwitchBase="br-lan.15"

RouterLOC="DEFAULT"
SwitchLOC="DEFAULT"
SwitchLOCT="DEFAULT"
ClientsLOC=""






LEVEL_ONE="   "
LEVEL_TWO="      "


Array=""
Old=""
Current=""

echo "  /* BEGIN NETWORK MAPPING */
  subgraph $ROOT_NAME {
    bgcolor="white"
    label=\"Network\""
for i in $(cat $FileLOC); do
	Current=$(echo "$i" | cut -d',' -f7)
	zeHostName="$(echo "$i" | cut -d',' -f1)"
	zeIp="$(echo "$i" | cut -d',' -f2)"
	tmpName=$(echo "$ROOT_NAME" | tr '_' '\n' | cut -c 1 | tr -d '\n' | tr '.' '_')
	preID="$(echo $tmpName$Current | tr '.' '_')_"
	tmpID=""


	if [[ "$Current" == "HW" ]]; then
		continue
	fi

	if [[ "$zeHostName" == "" ]]; then
		tmpID="$(echo $preID$RANDOM | tr '.' '_' | tr '-' '_')"
	else
		tmpID="$(echo $preID$zeHostName | tr '.' '_' | tr '-' '_')"
	fi

	if [[ "$RouterBase" == "$Current" ]] && [[ "$RouterLOC" == "DEFAULT" ]]; then
		RouterLOC="$tmpID"
	fi

	if [[ "$SwitchBase" == "$Current" ]] && [[ "$SwitchLOC" == "DEFAULT" ]]; then
		SwitchLOC="$tmpID"
	fi

	if [[ "$SwitchBase" == "$Current" ]]; then
		SwitchLOCT="$tmpID"
	fi



	if [[ "$RouterBase" != "$Current" ]] && [[ "$SwitchBase" != "$Current" ]]; then
		if [[ -z "$ClientsLOC" ]]; then
			ClientsLOC="$tmpID,$ROOT_NAME$(echo _$Current | tr '.' '_' | tr '-' '_')"
#			ClientsLOC="$tmpID"
		else
			yeep="$(echo $Current | tr '.' '_' | tr '-' '_')"
#			echo " ,,,,,,,, $yeep ??? '$(echo $ClientsLOC | grep -o $yeep | head -1)'"
			if [[ "$(echo $ClientsLOC | grep -o $yeep | head -1)" != "$yeep" ]]; then
#				echo "$ROOT_NAME$(echo _$Current | tr '.' '_' | tr '-' '_')"
				ClientsLOC="$ClientsLOC $tmpID,$ROOT_NAME$(echo _$Current | tr '.' '_' | tr '-' '_')"
			fi
		fi
	fi

	if [[ -z "$Old" ]]; then
		Old=$Current
		echo "$LEVEL_ONE subgraph Cluster_Devices_$Current {" | tr '.' '_' | tr '-' '_'
		colorC "$Current"
		echo "$LEVEL_TWO label=\"$nameOhName\""
		echo "$LEVEL_TWO color=\"$SetColorC\""
		echo "$LEVEL_TWO $tmpID [shape=box,color=black,label=\"$zeHostName\n$zeIp\"];"
	fi

	if [[ -z "$Array" ]]; then
		Array="$tmpID"
	fi

	if [[ "$Old" != "$Current" ]]; then
		Old=$Current
		if [[ -z $(echo "$Array" | grep -o '-') ]]; then
			echo "$LEVEL_TWO $Array"
		else
			echo "$LEVEL_TWO $Array [style=invis]"
		fi
		Array="$tmpID"
		echo "$LEVEL_ONE }"
#		echo "$Old"-"$Current !="
		echo "$LEVEL_ONE subgraph Cluster_Devices_$Current {" | tr '.' '_' | tr '-' '_'
		nameON "$Current"
		echo "$LEVEL_TWO label=\"$nameOhName\""
		colorC "$Current"
		echo "$LEVEL_TWO color=\"$SetColorC\""
		echo "$LEVEL_TWO $tmpID [shape=box,color=black,label=\"$zeHostName\n$zeIp\"];"
	else
#		echo "$Old"-"$Current =="
		echo "$LEVEL_TWO $tmpID [shape=box,color=black,label=\"$zeHostName\n$zeIp\"];"
		Array="$Array -> $tmpID"
	fi

done

echo "$LEVEL_TWO $Array [style=invis]"
echo "    }

    $RouterLOC -> $SwitchLOC [dir=\"back\",ltail=\"$ROOT_NAME$(echo _$RouterBase | tr '.' '_' | tr '-' '_')\"];"

ltail="$ROOT_NAME$(echo _$SwitchBase | tr '.' '_' | tr '-' '_')"
for i in $ClientsLOC; do
	client="$(echo $i | cut -d',' -f1)"
	lhead="$(echo $i | cut -d',' -f2)"
#	echo "'$i'"
	echo "    $SwitchLOCT -> { $client } [dir=\"back\",ltail=\"$ltail\",lhead=\"$lhead\"];"
#	echo "    $SwitchLOCT -> { $ClientsLOC } [dir=\"back\",ltail=\"$ltail\"];"

done

#    $SwitchLOCT -> { $ClientsLOC } [dir=\"back\",ltail=\"$ROOT_NAME$(echo _$SwitchBase | tr '.' '_' | tr '-' '_')\"];


echo "  }
  /*END NETWORK MAPPING*/"

