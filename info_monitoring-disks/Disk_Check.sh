#!/bin/bash

while [ ! -z "$1" ]; do			# This helps with the extra weird spaces ID: WWS
while getopts d:f:h:s:c:p:u:w:t:n: opt; do
	case $opt in
		d ) DISK="$OPTARG" ;;
		f ) FILE="$OPTARG" ;;
		h ) CHOSTNAME="$OPTARG" ;;
		s ) SERVER="$OPTARG" ;;
		c ) CLIENTID="$OPTARG" ;;
		p ) PRIORITY="$OPTARG" ;;
		u ) USER_NAME="$OPTARG" ;;
		w ) PASS="$OPTARG" ;;
		t ) TOPIC="$OPTARG" ;;
		n ) NOTIFICATION="$OPTARG" ;;
		: ) echo "ERROR: $OPTARG" ;;
	esac
done
shift					# Part ID: WWS-a
done
shift "$((OPTIND -1))"



if [[ -z "$DISK" ]] || [[ ! -d "$DISK" ]]; then
	read -p "Set Disk to monitor: " DISK
fi

if [[ -z "$FILE" ]]; then
	read -p "Test file to monitor: " FILE
fi

if [[ -z "$CHOSTNAME" ]]; then
	CHOSTNAME=$(uname -n)
fi

if [[ -z "$SERVER" ]]; then
	read -p "Enter Server location: " SERVER
fi

if [[ "$NOTIFICATION" == "Gotify" ]]; then
	if [[ -z "$CLIENTID" ]]; then
		read -p "Enter Client ID: " CLIENTID
	fi
else
	if [[ -z "$USER_NAME" ]]; then
		read -p "Enter Username: " USER_NAME
	fi

	if [[ -z "$PASS" ]]; then
		read -p "Enter Password: " PASS
	fi

	if [[ -z "$TOPIC" ]]; then
		read -p "Enter Topic: " TOPIC
	fi

fi

if [[ -z "$PRIORITY" ]]; then
	read -p "Enter Priority: " PRIORITY
fi


function SendMsg () {
	if [[ "$NOTIFICATION" == "Gotify" ]]; then
		curl "$SERVER/message?token=$CLIENTID" -F title="$CHOSTNAME - $1" -F message="$(date +%c): $DISK" -F priority="$PRIORITY"
	else
		curl -u "$USER_NAME:$PASS" "$SERVER/$TOPIC/publish?title=$(echo -n $CHOSTNAME - $1 | tr ' ' '+')&message=$(echo -n $(date +%c): $DISK | tr ' ' '+' | sed 's/:/%3A/g')&priority=$PRIORITY&tags=$2"
	fi
}


#echo "$DISK ; $FILE ; $CHOSTNAME ; $SERVER ; $CLIENTID ; $PRIORITY" | tee /home/user/here

if [[ ! -f "$DISK/$FILE" ]]; then
	SendMsg "DRIVE DOWN!" "warning"
	mount -a
	sleep 15
	if [[ -f "$DISK/$FILE" ]]; then
		PRIORITY="3"
		SendMsg "Drive is back up :)" "partying_face,white_check_mark"
	else
		SendMsg "Test failed. Check IMMEDIATELY" "rotating_light,warning"
	fi
fi
