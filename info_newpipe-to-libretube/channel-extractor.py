#!/bin/python3

import sys
import sqlite3



dbfile = sys.argv[1]
con = sqlite3.connect(dbfile)

cur = con.cursor()
cur.execute("SELECT feed_group_subscription_join.group_id, feed_group_subscription_join.subscription_id, subscriptions.uid, subscriptions.url, feed_group.name FROM feed_group_subscription_join, subscriptions, feed_group WHERE feed_group_subscription_join.subscription_id == subscriptions.uid AND feed_group.uid == feed_group_subscription_join.group_id ORDER BY feed_group.name;")


sqlData=cur.fetchall()


all={}

for data in sqlData:
  if not (data[4] == "name"):

    if data[4] in all.keys():
#      all[data[4]] = data[3].split("/")[4]
      tmpData = all[data[4]]
      all[data[4]] = tmpData+"|"+data[3].split("/")[4]
    else:
      all[data[4]] = data[3].split("/")[4]


print("{\"channelGroups\":[", end="")
end=len(all)
start=0
for i in all:
  a=i.strip()
  b=all[i.strip()].replace("|", "\",\"")

  if start == end-1:
    print("{\"name\": \"",a,"\",\"channels\": [\"", b, "\"]}", end="")
  else:
    print("{\"name\": \"",a,"\",\"channels\": [\"", b, "\"]},", end="")

  start += 1
print("]}")
